package TestScripts;

import org.testng.annotations.Test;



import Configurationfile.ConfigFile;

/**
 * Login as Invalid Credential
 * Verify Unable to login
 * Login as Valid Credential
 * perform search( we can search on the basis of CIF No, Account No, Passport No, Mobile No etc)
 * Verify Search Result
 */
     
    public class Demo extends ConfigFile{
	
	@Test(priority=1)

	public void invalidlogin() throws InterruptedException{
	loginHelper.Invalidlogincredential();
	loginHelper.clickonloginbutton();
	loginHelper.verifyError();
	}
	
	@Test(priority=2)
	
	public void login() throws InterruptedException{
	loginHelper.verifyloginpage();
	loginHelper.logincredential();
	loginHelper.clickonloginbutton();
	loginHelper.clickonSnappforStaff();
	searchCriteriaHelper.verifySearchCriteriaOption();
	searchCriteriaHelper.searchByOption("Account_Number");
	searchResultsHelper.verifySearchResultsScreen();
	}	
}
