package TestScripts;

import org.testng.annotations.Test;

import Configurationfile.ConfigFile;
import PageHelper.LoginHelper;

public class Demo2 extends ConfigFile{
	
	/**
	 * Login as Valid Credential
	 * Verify Search Criteria
	 * Perform search using desired CIF no
	 * Click on Select this Customer
	 * Click on Services > application show quick links to key services
	 */
	
	@Test
	public void scenario() throws Throwable {
	loginHelper.verifyloginpage();
	loginHelper.logincredential();
	loginHelper.clickonloginbutton();
	loginHelper.clickonSnappforStaff();
	searchCriteriaHelper.verifySearchCriteriaOption();
	searchCriteriaHelper.retrieveCusInfo();
	searchCriteriaHelper.enterCIFNUMBER();
	searchResultsHelper.verifySearchResultsScreen();
	searchResultsHelper.clickonForCursor();
	customerSelectionHelper.retrieveCusInfo();
	customerSelectionHelper.clickOnButton();
	customer360Helper.verifykeyServices();
	customer360Helper.clickonServices();
	customer360Helper.clickonSignOut();
	}

}
