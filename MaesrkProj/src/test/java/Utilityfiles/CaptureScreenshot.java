package Utilityfiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;

public class CaptureScreenshot {
	
	/**
	 * This function capture the screenshot for failed test cases
	 * @param driver
	 * @param filename
	 */
	static String screenshotName="";
	
	public void captureFailedTCScreenshot(AppiumDriver driver, String filename) throws IOException, InterruptedException{
		
		screenshotName= filename+getCurrentTime();
		
		try {
			FileOutputStream out = new FileOutputStream("screenshot//"+screenshotName+".jpg");
				
			out.write(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES));
			out.close();
		
			String screen = "file://"+getPath()+"/screenshot/"+screenshotName+".jpg";
			System.out.println("screen "+screen);
			String str = "<a href= '" + screen+ "    "+ "'target='_blank' >" + filename + "</a>";

			Reporter.log(str);		
			
			
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		} catch (WebDriverException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * This function get the project absolute path
	 * @return Project absolute path
	 */
	public String getPath(){
		String path ="";
		File file = new File("");
		String absolutePathOfFirtFile=file.getAbsolutePath();
		path = absolutePathOfFirtFile.replace("\\\\+", "/");
		return path;
	}
	
	public String getCurrentTime(){
		DateFormat format = new SimpleDateFormat("ddMMyyHHmmss");
		Date date = new Date();
		return format.format(date);
	}
	

	public void cleanDir() throws IOException, InterruptedException{
		String projectDir = System.getProperty("user.dir");
		String screenshotDir = projectDir + "\\screenshot";
		String surefiretargetDir = projectDir + "\\target\\surefire-reports";
		FileUtils.deleteDirectory(new File(screenshotDir));
		File file1 = new File(System.getProperty("user.dir")+"//target//surefire-reports");
		file1.delete();
		Thread.sleep(3000);
		File theDir = new File("screenshot");			
		theDir.mkdir();
		System.out.println("Clean the screenshot directory");
		Thread.sleep(3000);
		FileUtils.deleteDirectory(new File(surefiretargetDir));
		System.out.println("Clean the olds Screenshots");
		
	}
	
	public void copyScreenshotIntoSurefire() throws IOException, InterruptedException{
		Thread.sleep(5000);
		String projectDir = System.getProperty("user.dir");
		String screenshotDir = projectDir + "\\screenshot";
		String surefiretargetDir = projectDir + "\\target\\surefire-reports";
		String screenshotBackupDir = projectDir + "\\back_screenshot";
		Process prs1 = Runtime.getRuntime().exec("cmd.exe /C copy /y " + screenshotDir+"\\*.jpg " +  surefiretargetDir);
		prs1.waitFor();
		System.out.println("Copying screenshots to " + screenshotBackupDir);
		Process prs = Runtime.getRuntime().exec("cmd.exe /C copy /y " + screenshotDir+"\\*.jpg " +  screenshotBackupDir);
		prs.waitFor();
		System.out.println("Copying screenshots to " + screenshotBackupDir);
	}

}

