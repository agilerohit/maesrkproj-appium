package Utilityfiles;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReadTestData {

	String path = getPath();
	String server = System.getProperty("server");

	/**
	 * This function get the project absolute path
	 * 
	 * @return Project absolute path
	 */
	public String getPath() {
		String path = "";
		File file = new File("");
		String absolutePathOfFirtFile = file.getAbsolutePath();
		path = absolutePathOfFirtFile.replace("\\\\+", "/");
		return path;
	}
	
	/**
	 * This function generate the random number with current date and time.
	 * @return random number
	 */
	public String getRandomNumber(){
		DateFormat format = new SimpleDateFormat("ddMMyyHHmmss");
		Date date = new Date();
		return format.format(date);
		}

	
	//Read property file
	public static String readPropertyFile(String key)
	{ 

	String value = "";
	try{            
	Properties prop = new Properties();
	File f = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Utilityfiles\\testData.properties");
	if(f.exists()){
	prop.load(new FileInputStream(f));
	value = prop.getProperty(key); 
	}
	}
	catch(Exception e){  
	System.out.println("Failed to read from Properties file.");  
	}
	return value;
	} 


}
