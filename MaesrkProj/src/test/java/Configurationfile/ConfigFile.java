package Configurationfile;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import PageHelper.Customer360Helper;
import PageHelper.CustomerSelectionHelper;
import PageHelper.SearchCriteriaHelper;
import PageHelper.LoginHelper;
import PageHelper.SearchResultsHelper;
import PageHelper.UserDetailsHelper;
import Utilityfiles.CaptureScreenshot;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public abstract class ConfigFile {

	protected static AndroidDriver driver;
	protected LoginHelper loginHelper;
	protected SearchCriteriaHelper searchCriteriaHelper;
	protected SearchResultsHelper searchResultsHelper;
	protected UserDetailsHelper userDetailsHelper;
	protected CustomerSelectionHelper customerSelectionHelper;
	protected Customer360Helper customer360Helper;

	/**
	 * This function clean the screenshot directory.
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@BeforeClass
	public void cleanScreenShotDirectory() throws IOException, InterruptedException {
		System.out.println("*********************************************");
		System.out.println("Executing Test Script Class File Name: " + this.getClass().getSimpleName());
		System.out.println("*********************************************");
		CaptureScreenshot captureScreenshots = new CaptureScreenshot();
		captureScreenshots.cleanDir();
	}

	/**
	 * setUp function defined the capabilities Create Appium Driver object.
	 * 
	 * @throws MalformedURLException
	 */
	@BeforeMethod
	public void setUp(Method method) throws MalformedURLException {
		System.out.println("*********************************************");
		System.out.println("Test Method Name: " + method.getName());
		System.out.println("*********************************************");
		String device = System.getProperty("device");
		// String appPath = System.getProperty("app");
		String udid = System.getProperty("udid");
		String port = System.getProperty("port");
		if (port == null)
			port = "4723";
		System.out.println("(and) port number=" + port);
		String appPath = System.getProperty("user.dir");
		System.out.println("appPath: " + appPath);
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("appium-version", "1.4.16.1");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "TA99301YYA");
		capabilities.setCapability("fullReset", true);
		capabilities.setCapability("newCommandTimeout", 120);
		/* capabilities.setCapability("app", appPath); */
		capabilities.setCapability("app", appPath + "\\APP\\snapp-ubo.apk");
		capabilities.setCapability("appPackage", "com.vipera.ts.starter.MashreqUBO");
		capabilities.setCapability("appActivity", "com.vipera.dynamicengine.DESplashActivity");
		capabilities.setCapability("unicodeKeyboard", "true");
		capabilities.setCapability("resetKeyboard", "true");
		capabilities.setCapability("app-wait-activity", ".MainActivity");
		if (device.contains("device")) {
			capabilities.setCapability("udid", udid);
		}

		driver = new AndroidDriver(new URL("http://127.0.0.1:" + port + "/wd/hub"), capabilities) {

			public MobileElement scrollToExact(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			public MobileElement scrollTo(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		initializePageHelperObjects();
		// configFile.setNetworkConnection(false, true, true);
	}

	/**
	 * In After method quit the Appium driver.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */
	@AfterMethod
	public void teardown(ITestResult result) throws InterruptedException, IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			String testName = result.getMethod().getMethodName();
			CaptureScreenshot captureScreenShots = new CaptureScreenshot();
			captureScreenShots.captureFailedTCScreenshot(driver, testName);
		}
		driver.quit();
	}

	@AfterClass
	public void copyScreenshotToSureFire() throws IOException, InterruptedException {
		CaptureScreenshot captureScreenShots = new CaptureScreenshot();
		captureScreenShots.copyScreenshotIntoSurefire();
		cleanUpPageHelperObjects();
	}

	/**
	 * @return appium driver object
	 */
	public static AndroidDriver getAppiumDriver() {
		return driver;
	}

	public void initializePageHelperObjects() {

		loginHelper = new LoginHelper(driver);
		searchCriteriaHelper = new SearchCriteriaHelper(driver);
		searchResultsHelper = new SearchResultsHelper(driver);
		userDetailsHelper = new UserDetailsHelper(driver);
		customerSelectionHelper = new CustomerSelectionHelper(driver);
		customer360Helper = new Customer360Helper(driver);

	}

	public void cleanUpPageHelperObjects() {

		loginHelper = null;
		searchCriteriaHelper = null;
		searchResultsHelper = null;
		userDetailsHelper = null;
		customerSelectionHelper = null;
		customer360Helper = null;

	}

}
