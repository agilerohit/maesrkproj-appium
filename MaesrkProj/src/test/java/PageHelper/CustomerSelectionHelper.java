package PageHelper;

import io.appium.java_client.AppiumDriver;
import Utilityfiles.DriverHelper;
import Utilityfiles.ReadTestData;

public class CustomerSelectionHelper extends DriverHelper{

	public CustomerSelectionHelper(AppiumDriver wd) {
	super(wd);
	}
	
	//Locators
	String dob = "//android.view.View[contains(@text,'DATE OF BIRTH')]";
	String mobno = "//android.view.View[@resource-id='tab2']/android.view.View[1]/android.view.View[5]";
			//"//android.view.View[@text='0501234567']";
	String emailaddress = "//android.view.View[contains(@text,'vgauba@aol.com')]";
	String selectCustBtn ="//android.view.View/android.view.View/android.view.View[8]/android.widget.Button";
			//"//android.widget.Button[contains(@text,'SELECT THIS CUSTOMER')]";
	
	//This method retrieve Date of Birth, Mobile no & Email address
	public void retrieveCusInfo()
	{   
		//WaitForElementPresent(mobno, 10);
		//String gettxt = getText(mobno);
	    //System.out.println(gettxt);
		//getText(mobno).equals(ReadTestData.readPropertyFile("Mobile_No"));
		//System.out.println("Mobile No is correct");
		String emailaddrs = getText(emailaddress);
	    System.out.println(emailaddrs);
		getText(emailaddress).equals(ReadTestData.readPropertyFile("Email_Address"));
		System.out.println("Email Address is correct");
	}
	
	//This method click on SELECT THIS CUSTOMER button
	public void clickOnButton() throws InterruptedException
	{
		Thread.sleep(2000);
	WaitForElementPresent(selectCustBtn, 20);
	clickOn(selectCustBtn);
	}

}
