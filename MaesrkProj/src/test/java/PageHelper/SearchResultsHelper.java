package PageHelper;

import org.testng.Assert;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;
import Utilityfiles.DriverHelper;

public class SearchResultsHelper extends DriverHelper{

	public SearchResultsHelper(AppiumDriver wd) {
	super(wd);
	}
	
	//Locators
	String matchresultfound = "//android.view.View[contains(@text,'1 matches found')]";
	String customername = "//android.view.View[@resource-id='tab2']/android.view.View[1]/android.view.View[1]";
	String ciflbl = "//android.view.View[contains(@text,'CIF')]";
	String cifno = "//android.view.View[@resource-id='tab2']/android.view.View/android.view.View[2]/android.view.View[2]";
	String nationalitylbl = "//android.view.View[@text='NATIONALITY']";
	String nationalitylogo = "//android.view.View[@resource-id='tab2']/android.view.View/android.view.View[2]/android.view.View[4]";
	String segmentlbl = "//android.view.View[@text='SEGMENT']";
	String segmentname = "//android.view.View[@resource-id='tab2']/android.view.View/android.view.View[2]/android.view.View[6]";
	String typelbl = "//android.view.View[@text='TYPE']";
	String retail = "//android.view.View[@resource-id='tab2']/android.view.View/android.view.View[2]/android.view.View[6]";
	
	
	//This method will verify Search Results Screen
	public void verifySearchResultsScreen()
	
	{
	 Assert.assertTrue(isElementPresent(matchresultfound));								    Reporter.log("Search Result is found", true);
	 Assert.assertTrue(isElementPresent(customername)); 							        Reporter.log("Customer name is found", true);
	 Assert.assertTrue(isElementPresent(ciflbl));                                           Reporter.log("CIF label is found", true);
	 Assert.assertTrue(isElementPresent(cifno));                                            Reporter.log("CIF NO is found", true);
	 Assert.assertTrue(isElementPresent(nationalitylbl));							        Reporter.log("NATIONALTY label is found", true);
	 Assert.assertTrue(isElementPresent(nationalitylogo));							        Reporter.log("NATIONALTY logo is found", true);
	 Assert.assertTrue(isElementPresent(segmentlbl));							            Reporter.log("SEGMENT label is found", true);
	 Assert.assertTrue(isElementPresent(segmentname));							            Reporter.log("GOLD is found", true);
	 Assert.assertTrue(isElementPresent(typelbl));							                Reporter.log("TYPE label is found", true);
	 Assert.assertTrue(isElementPresent(retail));							                Reporter.log("RETAIL is found", true);
	}
	
	//This method will click on forward cursor
	public void clickonForCursor()
	
	{
	 //WaitForElementPresent(forCursor, 30);
	 clickOn(customername);
	}
	
	
	

}
