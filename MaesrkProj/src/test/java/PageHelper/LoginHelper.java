package PageHelper;

import org.testng.Assert;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;
import Utilityfiles.DriverHelper;
import Utilityfiles.ReadTestData;

public class LoginHelper extends DriverHelper{

	public LoginHelper(AppiumDriver wd) {
		super(wd);
	}

	//Locators
	String username = "//android.widget.EditText[contains(@text,'USERNAME')]";
	String password = "//android.widget.EditText[contains(@text,'PASSWORD')]";
	String branch = "//android.widget.EditText[contains(@text,'TYPE NAME OF YOUR BRANCH')]";
	String checkbox = "//android.widget.CheckBox";
	String signon = "//android.widget.Button";
	String branchedit = "//android.view.View[contains(@text,'JLT Branch')]";
	String popmessgtxt = "//android.view.View[contains(@text,'User Not Allowed')]";
	String canButtn = "//android.widget.Image[contains(@text,'cancel')]";	   

	//Verify login page method
	public void verifyloginpage(){
		Assert.assertTrue(isElementPresent(username));								            Reporter.log("User Name is found", true);
		Assert.assertTrue(isElementPresent(password)); 							                Reporter.log("Password is found", true);
		Assert.assertTrue(isElementPresent(branch));                                            Reporter.log("Branch is found", true);
		Assert.assertTrue(isElementPresent(checkbox));                                          Reporter.log("Check Box is found", true);
		Assert.assertTrue(isElementPresent(signon));							                Reporter.log("Sign On Button is found", true);
	}

	//enter login with valid credentials
	public void logincredential() throws InterruptedException
	{
		clickOn(username);
		sendKeys(username, ReadTestData.readPropertyFile("loginid"));
		clickOn(password);
		sendKeys(password, ReadTestData.readPropertyFile("password"));
		clickOn(branch);
		sendKeys(branch, ReadTestData.readPropertyFile("type"));
		clickOn(branchedit);
		clickOn(checkbox);
	}

	//enter login with valid credentials
	public void Invalidlogincredential() throws InterruptedException
	{
		clickOn(username);
		sendKeys(username, ReadTestData.readPropertyFile("wrongloginid"));
		clickOn(password);
		sendKeys(password, ReadTestData.readPropertyFile("wrongpassword"));
		clickOn(branch);
		sendKeys(branch, ReadTestData.readPropertyFile("type"));
		clickOn(branchedit);
		clickOn(checkbox);
		Reporter.log("Invalid Credentials");
	}


	public void clickonloginbutton()
	{
		clickOn(signon);
		Reporter.log("User Logged In");
	}

	//This method verify error screen for Invalid login then enter valid username
	public void verifyError()
	{
		Assert.assertTrue(isElementPresent(popmessgtxt));
		Assert.assertTrue(isElementPresent(canButtn));
		//Dismiss error popup
		clickOn(canButtn);
		Reporter.log("Error message Shown");
	}



	//DashBoard Screen 

	//Locator
	String snappforstaff = "//android.view.View[@text='SNAPP FOR STAFF']";

	//Method
	public void clickonSnappforStaff()
	{
		clickOn(snappforstaff);
	}

}
