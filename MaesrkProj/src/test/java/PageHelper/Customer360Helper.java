package PageHelper;

import org.testng.Assert;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;
import Utilityfiles.DriverHelper;

public class Customer360Helper extends DriverHelper{

	public Customer360Helper(AppiumDriver wd) {
	super(wd);
	
	}
	
	//Locator
	String services = "//android.view.View[contains(@text,'SERVICES')]";
	String KeyServices = "//android.view.View[contains(@text,'QUICK LINKS TO KEY SERVICES')]";
	String signOut = "//android.view.View[contains(@text,'SIGN OUT')]";
	
	//This method click on Services
	public void clickonServices()
	{
	WaitForElementPresent(KeyServices, 20);
	clickOn(services);
	}
	
	//This method verify QUICK LINKS TO KEY SERVICES 
	public void verifykeyServices() throws Throwable
	{
	Thread.sleep(5000);	
	WaitForElementPresent(KeyServices, 20);
	Assert.assertTrue(isElementPresent(KeyServices));								  Reporter.log("QUICK LINKS TO KEY SERVICES", true);
	}
	
	//This method click on Sign Out button
	public void clickonSignOut()
	{
	clickOn(signOut);
	}

}
