package PageHelper;

import org.testng.Assert;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;
import Utilityfiles.DriverHelper;

public class UserDetailsHelper extends DriverHelper {

	public UserDetailsHelper(AppiumDriver wd) {
	super(wd);
	}
	
	//Hard coded data
	String Email = "vgauba@aol.com";
	
	//Locators
	String maleIcon = "";
	String cusName = "";
	String actIcon = "";
	String gold = "";
	String cifno = "";
	String nationalitylbl = "";
	String mobile = "";
	String lbnationalitylogo = "";
	String mobNo = "";
	String emaillbl = "";
	String email = "";
	String relaManagerlbl = "";
	String relaManagerval= "";
	String namlbl = "";
	String any = "";
	
	//This method will verify User Details
	public void verifyUserDetails()
	{
		Assert.assertTrue(isElementPresent(maleIcon));								        Reporter.log("Male Icon is found", true);
		Assert.assertTrue(isElementPresent(cusName)); 							            Reporter.log("Customer name is found", true);
		Assert.assertTrue(isElementPresent(actIcon));                                       Reporter.log("Action Icon is found", true);
		Assert.assertTrue(isElementPresent(gold));                                          Reporter.log("GOLD is found", true);
		Assert.assertTrue(isElementPresent(cifno));							                Reporter.log("CIF NO is found is found", true);
		Assert.assertTrue(isElementPresent(nationalitylbl));							    Reporter.log("NATIONALTY label is found", true);
		Assert.assertTrue(isElementPresent(lbnationalitylogo));							    Reporter.log("NATIONALTY logo is found", true);
		Assert.assertTrue(isElementPresent(mobNo));							                Reporter.log("Mobile No is found", true);
		Assert.assertTrue(isElementPresent(emaillbl));							            Reporter.log("Email label is found", true);
		Assert.assertTrue(isElementPresent(relaManagerlbl));							    Reporter.log("Relational Manager label is found", true);
		Assert.assertTrue(isElementPresent(relaManagerval));								Reporter.log("Relational Manager value is found", true);
		Assert.assertTrue(isElementPresent(namlbl)); 							            Reporter.log("Customer name is found", true);
		Assert.assertTrue(isElementPresent(any));                                           Reporter.log("ANY text is found", true);
		Assert.assertTrue(isElementPresent(email));                                         Reporter.log("Email address is found", true);
	}
}
