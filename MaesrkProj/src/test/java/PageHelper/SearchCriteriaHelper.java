package PageHelper;

import org.testng.Assert;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;
import Utilityfiles.DriverHelper;
import Utilityfiles.ReadTestData;

public class SearchCriteriaHelper extends DriverHelper{

	public SearchCriteriaHelper(AppiumDriver wd) {
	super(wd);
	}
		
	//Locators
	String byCriteriatxt = "//android.view.View[@text='by Criteria]";
	String backButton = "//android.widget.Image[@text='back_ubo']";
	String userinControltxt= "//android.view.View[@text='USER IN CONTROL']";
	String signOut = "//android.view.View[@text='SIGN OUT']";
	String cifNotxt = "//android.view.View[@text='CIF NUMBER']";
	String accountNotxt = "//android.view.View[@text='ACCOUNT NUMBER']";
	String creditcardNotxt = "//android.view.View[@text='CREDIT CARD NUMBER']";
	String passportNotxt = "//android.view.View[@text='PASSPORT NUMBER']";
	String customerNametxt = "//android.view.View[@text='CUSTOMER NAME']";
	String mobileNo = "//android.view.View[@text='MOBILE NUMBER']";
	String tradeLicNotxt = "//android.view.View[@text='TRADE LICENSE NUMBER']";
	String emiratesIdNotxt = "//android.view.View[@text='EMIRATES ID NUMBER']";
	String emiratedIdscantxt = "//android.view.View[@text='EMIRATES ID SCAN']";
	String CIFtxtbox = "//android.widget.EditText[contains(@text,'Enter Customer CIF')]";
	String MOBtxtbox = "//android.widget.EditText[@text='Enter Customer Mobile No.']";
	String Accountxtbox = "//android.widget.EditText[@text='Enter Customer A/C No.']";
	String PASStxtbox = "//android.widget.EditText[@text='Enter Customer PP No.']";
	String cusNtxtbox = "//android.widget.EditText[@text='Enter Customer Name']";
	String searchIcon ="//android.widget.Image[@text='search_id_2']";
	
	
	
	//Methods
	public void verifySearchCriteriaScreenHeader()
	{
		 Assert.assertTrue(isElementPresent(byCriteriatxt));								    Reporter.log("By Criteria Text is found", true);
		 Assert.assertTrue(isElementPresent(backButton));								        Reporter.log("Back Button is found", true);
		 Assert.assertTrue(isElementPresent(userinControltxt));								    Reporter.log("USER CONTROL text is found", true);
		 Assert.assertTrue(isElementPresent(signOut));								            Reporter.log("SIGN OUT is found", true);
	}
	
	//This method will verify option available for search criteria
	public void verifySearchCriteriaOption()
	{
	    Assert.assertTrue(isElementPresent(cifNotxt));								            Reporter.log("CIF NUMBER is found", true);
		Assert.assertTrue(isElementPresent(accountNotxt)); 							            Reporter.log("ACCOUNT NUMBER is found", true);
		Assert.assertTrue(isElementPresent(creditcardNotxt));                                   Reporter.log("CREDIT CARD NUMBER is found", true);
		Assert.assertTrue(isElementPresent(passportNotxt));                                     Reporter.log("PASSPORT NUMBER is found" , true);
		Assert.assertTrue(isElementPresent(customerNametxt));							        Reporter.log("CUSTOMER NAME is found", true);
		Assert.assertTrue(isElementPresent(mobileNo));                                          Reporter.log("MOBILE NUMBER is found", true);
		Assert.assertTrue(isElementPresent(tradeLicNotxt));                                     Reporter.log("TRADE LICENSE NUMBER is found", true);
		Assert.assertTrue(isElementPresent(emiratesIdNotxt));                                   Reporter.log("EMIRATES ID NUMBER is found", true);
		Assert.assertTrue(isElementPresent(emiratedIdscantxt));                                 Reporter.log("EMIRATES ID SCAN is found", true);
	}
	
	//
	public void searchByOption(String key) 
	{
		//Search criteria on the basis of CIF no
		if(key=="CIF_Number")
		{ 
		enterCIFNUMBER();
		
		}
		//Search criteria on the basis of Mobile no
		else if(key=="Mobile_No")
		{ 
		enterMOBNUMBER();	
		}
		//Search criteria on the basis of Passport no
		else if(key=="Passport_Number")
		{ 
		enterPassportNumber();	
		}
		//Search criteria on the basis of Customer Name
		else if(key=="CustomerName")
		{ 
		enterCustomerName();	
		}
		//Search criteria on the basis of Account no
		else
		{
		enterAccounNumber();	
		}
	}
	
	
	
	
	//This method allows to enter only cif no in search box
	public void enterCIFNUMBER()
	{   
		WaitForElementPresent(CIFtxtbox, 10);
		clickOn(CIFtxtbox);
		sendKeys(CIFtxtbox, ReadTestData.readPropertyFile("CIF_Number"));
		clickOn(searchIcon);
	}
	
	   //This method allows to enter only cif no in search box
		public void enterMOBNUMBER()
		{   clickOn(mobileNo);
			sendKeys(MOBtxtbox, ReadTestData.readPropertyFile("Mobile_No"));
			clickOn(searchIcon);
		}
		
		//This method allows to enter only Account no in search box
		public void enterAccounNumber()
		{   clickOn(accountNotxt);
			sendKeys(Accountxtbox, ReadTestData.readPropertyFile("Account_Number"));
			clickOn(searchIcon);
		}
		
		//This method allows to enter only Passport no in search box
		public void enterPassportNumber()
		
		{   clickOn(passportNotxt);
			sendKeys(PASStxtbox, ReadTestData.readPropertyFile("Passport_Number"));
			clickOn(searchIcon);
		}
		
		//This method allows to enter only Passport no in search box
		public void enterCustomerName()
				
		{   clickOn(customerNametxt);
			sendKeys(cusNtxtbox, ReadTestData.readPropertyFile("CustomerName"));
			clickOn(searchIcon);
		}
	
	    //This method retrieve Date of Birth, Mobile no & Email address
		public void retrieveCusInfo()
		
		{   String gettxt = getText(mobileNo);
		    System.out.println(gettxt);
			getText(mobileNo).equals(ReadTestData.readPropertyFile("Mobile_No"));
			System.out.println("Mobile No is correct");
			
		}
		
		//This method will perform generic search
		public void genSearch(int input)
		
		{ 
		  
		  int ci = 010424124;
		  int mi = 0501234567;
		 
		  switch (input) 
		  {
		  case 1: input = ci;
		  new SearchCriteriaHelper(driver).enterCIFNUMBER();
		  System.out.println("Testing is going on");
		  //enterCIFNUMBER();
		  case 2: input = mi;
		  new SearchCriteriaHelper(driver).enterMOBNUMBER();
		  System.out.println("Testing is going on");
		  	  
		  }
			
		}
		
		
}
